import React from 'react';

export default class ProjectsCrud extends React.Component
{
    projectsList()
    {
        if (!this.props.projects) {
            return null;
        }

        let lines = [];

        for (let project_id in this.props.projects) {
            let project = this.props.projects[project_id];

            lines.push(
                <tr onClick = {() => this.props.viewProject(project_id)} key = {project_id}>
                    <td>{project_id}</td>
                    <td>{project.name}</td>
                    <td>{project.scenes_number}</td>
                    <td className = "actions">
                        <button type = "button" className = "btn btn-sm btn-secondary" onClick = {() => this.props.handleDisable(project_id)}>close</button>
                        <button type = "button" className = "btn btn-sm btn-danger" onClick = {() => this.props.handleDelete(project_id)}>delete</button>
                    </td>
                </tr>
            );
        }

        return lines;
    }

    render()
    {
        return (
            <table className="table col-11 table-hover">
                <thead className = "thead-dark table-borderless">
                    <tr>
                        <th scope = "col">#</th>
                        <th scope = "col">Name</th>
                        <th scope = "col">Scenes</th>
                        <th scope = "col"></th>
                    </tr>
                </thead>
                <tbody>
                    {this.projectsList()}
                </tbody>
            </table>
        );
    }
}
