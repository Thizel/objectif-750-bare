import React from 'react'
import axios from 'axios';

import ProjectForm from './project_form'
import ProjectsCrud from './projects_crud'
import * as Constants from '../includes/constants';

export default class Projects extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            mode: 'show',
            projects: null,
            project: null,
            message: {
                type: null,
                content: null
            }
        }
    }

    componentDidMount()
    {
        axios
            .get(
                `${Constants.api_url}/projects/read`,
                {headers: {Authorization: this.props.userToken}}
            )
            .then(res => this.setState({projects: res.data}));
    }

    changeMode(mode)
    {
        this.setState({mode: mode});
    }

    viewProject(project_id)
    {
        let project = this.state.projects[project_id];
        this.setState({mode: 'view', project: project})
    }

    projectView()
    {
        return (
            <section>
                <div>
                    <button
                        className = "btn btn-secondary"
                        onClick   = {() => this.changeMode('show')}>
                        Return
                    </button>
                </div>
                <h4>{this.state.project.name}</h4>
                <p>{this.state.project.description}</p>
            </section>
        );
    }

    projectSubmitted()
    {
        this.setState({
            mode: 'show',
            message: {
                type: 'success',
                content: 'Project has been created'
            }
        });
    }

    handleDelete(project_id)
    {
        axios
            .get(
                `${Constants.api_url}/projects/delete/${project_id}`,
                {'headers': {Authorization: this.props.userToken}}
            )
            .then(res => {
                let projects = this.state.projects.slice();
                delete projects[project_id];
                this.setState({projects: projects});
            });
    }

    handleDisable(project_id)
    {
        axios
            .get(
                `${Constants.api_url}/projects/disable/${project_id}`,
                {'headers': {Authorization: this.props.userToken}}
            )
            .then(this.props.return());
    }

    render()
    {
        switch (this.state.mode) {
            default:
            case 'show':
                return (
                    <section id = "projects_page">
                        <Message value = {this.state.message} />
                        <AddButton addProject = {() => this.changeMode('add')} />
                        <ProjectsCrud
                            projects      = {this.state.projects}
                            handleDelete  = {(project_id) => this.handleDelete(project_id)}
                            handleDisable = {(project_id) => this.handleDisable(project_id)}
                            viewProject   = {(project_id) => this.viewProject(project_id)}
                            return        = {() => this.changeMode('show')} />
                    </section>
                );

            case 'add':
                return (
                    <section id="projects_page">
                        <ProjectForm projectSubmitted = {this.projectSubmitted} />
                    </section>
                );

            case 'view':
                return this.projectView();
        }
    }
}

function AddButton(props)
{
    return (
        <p>
            <button
                className = "btn btn-primary"
                onClick   = {props.addProject}>
                New
            </button>
        </p>
    );
}

function Message(props)
{
    if (!props.value.content) {
        return null;
    }

    return <div className = {'col-9 alert alert-' + props.value.type} role = "alert">{props.value.content}</div>;
}
