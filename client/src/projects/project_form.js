import React from 'react';
import axios from 'axios';
import * as Constants from '../includes/constants';

const max_desc_length = 500;

export default class ProjectForm extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            name: '',
            description: '',
            charsLeft: max_desc_length,
        }

        this.countCharacters = this.countCharacters.bind(this);
        this.handleChange    = this.handleChange.bind(this);
        this.handleSubmit    = this.handleSubmit.bind(this);
    }

    countCharacters(event)
    {
        this.setState({charsLeft: (max_desc_length - event.target.value.length)})
    }

    handleChange(event)
    {
        this.setState({[event.target.name]: event.target.value});

        if (event.target.name === 'description') {
            this.countCharacters(event);
        }
    }

    handleSubmit(event)
    {
        event.preventDefault();
        let data = {};

        for (var i = 0; i < event.target.elements.length; i++) {
            if (event.target.elements[i].type !== 'submit') {
                data[event.target.elements[i].name] = event.target.elements[i].value;
            }
        }

        data = JSON.stringify(data);
        axios.post(
            `${Constants.api_url}/projects/submit`,
            data,
            {headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                Authorization: this.props.userToken,
            }}
        ).then(res => this.props.projectSubmitted());
    }

    render()
    {
        return (
            <div>
                <ReturnButton return = {this.props.return} />
                <form
                    onSubmit  = {this.handleSubmit}
                    className = "col-6">
                    <div className="form-group">
                        <label htmlFor="project_name">Project's name:</label>
                        <input
                            type      = "text"
                            name      = "name"
                            className = "form-control"
                            value     = {this.state.name}
                            onChange  = {this.handleChange} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="project_name">Quick description:</label>
                        <p className="small">{this.state.charsLeft} chars left - be concise!</p>
                        <textarea
                            rows      = "7"
                            name      = "description"
                            maxLength = {max_desc_length}
                            className = "form-control"
                            onChange  = {this.handleChange}
                            value     = {this.state.description} />
                    </div>
                    <div className="form-group">
                        <input
                            type      = "submit"
                            onClick   = {this.props.onClick}
                            className = "btn btn-success"
                            value     = "Save" />
                    </div>
                </form>
            </div>
        );
    }
}

function ReturnButton(props)
{
    return (
        <div>
            <button
                className = "btn btn-secondary"
                onClick   = {props.return}>
                Return
            </button>
        </div>
    );
}