import React from 'react';
import axios from 'axios';

import * as Constants from '../includes/constants';

export default class UserLogin extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            username: '',
            password: '',
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event)
    {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit(event)
    {
        event.preventDefault();
        let data = {};

        for (var i = 0; i < event.target.elements.length; i++) {
            if (event.target.elements[i].type !== 'submit') {
                data[event.target.elements[i].name] = event.target.elements[i].value;
            }
        }

        data = JSON.stringify(data);
        axios
            .post(
                `${Constants.public_url}/login`,
                data,
                {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Accept': 'application/json',
                    }
                }
            )
            .then(res => this.props.userLoggedIn(res.data))
            .catch(error => {
                console.log(error);
            });
    }

    render()
    {
        return (
            <div className = "container">
                <form
                    onSubmit  = {this.handleSubmit}
                    className = "col-4">
                    <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input
                            type      = "text"
                            name      = "username"
                            className = "form-control"
                            required
                            value     = {this.state.username}
                            onChange  = {this.handleChange} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input
                            type      = "password"
                            name      = "password"
                            className = "form-control"
                            required
                            value     = {this.state.password}
                            onChange  = {this.handleChange} />
                    </div>
                    <div className="form-group">
                        <input
                            type      = "submit"
                            className = "btn btn-success"
                            value     = "Submit" />
                    </div>
                </form>
            </div>
        );
    }
}