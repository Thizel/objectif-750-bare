import React from 'react'
import axios from 'axios';
import Stats from './stats';
import * as Constants from '../includes/constants';

export default class Words extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            content: '',
            word_count: 0,
            days_goal: 0,
            percentage_done: 0,
            idle: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount()
    {
        axios.get(
                `${Constants.api_url}/words/read`,
                {headers: {Authorization: 'Bearer ' + this.props.userToken}}
            )
            .then(res => this.setState({
                word_count: res.data.word_count ? Number.parseInt(res.data.word_count) : 0,
                content: res.data.content
            })
        );

        this.interval = setInterval(() => document.getElementById('words_form_submit').click(), 15000);
    }

    componentWillUnmount()
    {
        clearInterval(this.interval);
    }

    handleChange(event)
    {
        const value = event.target.value;
        const name = event.target.name;

        this.setState({
            [name]: value
        });

        if (name === 'content') {
            this.countWords(value);
        }
    }

    countWords(value)
    {
        const regexp = /[,\-?!.'";:*_/()]+/gm;
        const result = regexp[Symbol.replace](value, ' ').trim();
        let tab = result.split(' ');
        tab = tab.filter(function(n) { return n !== ''});
        this.setState({word_count: tab.length});
    }

    handleSubmit(event)
    {
        event.preventDefault();
        axios.post(`${Constants.api_url}/words/submit`,
            {content: document.getElementById('content').value},
            {headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                Authorization: 'Bearer ' + this.props.userToken
            }}
        );
    }

    render()
    {
        return (
            <section id="words_page" className="row">
                <aside className="stats col-3">
                    <Stats
                        todays_word_count = {this.state.word_count}
                        days_goal         = {this.state.days_goal}
                        percentage_done   = {this.state.percentage_done}
                        userToken         = {this.props.userToken}
                    />
                </aside>
                <section className="words col-9">
                    <p className="word_count">{this.state.word_count}</p>

                    <form onSubmit={this.handleSubmit}>
                        <div className = "form-group">
                            <textarea
                                id        = "content"
                                name      = "content"
                                className = "form-control"
                                rows      = "5"
                                autoFocus
                                value     = {this.state.content}
                                onChange  = {this.handleChange} />
                        </div>
                        <input id="words_form_submit" type="submit" value="Submit" />
                    </form>
                </section>
            </section>
        );
    }
}
