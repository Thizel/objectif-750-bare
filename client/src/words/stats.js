import React from 'react'
import axios from 'axios';
import * as Constants from '../includes/constants';

export default class Stats extends React.Component {
    constructor(props)
    {
        super(props);

        this.state = {
            month_wc: 0,
            month_objective: 0,
            is_month_obj_reached: false,
            day_wc: 0,
            day_goal: 0,
            day_percentage: 0,
            is_day_obj_reached: false,
        }
    }

    componentDidMount()
    {
        axios.get(
                `${Constants.api_url}/stats/read`,
                {'headers': {Authorization: this.props.userToken}}
            )
            .then(res => {
                const day_wc = res.data.day_wc ? Number.parseInt(res.data.day_wc) : 0
                const day_goal = res.data.day_goal ? Number.parseInt(res.data.day_goal) : 0;
                const day_percentage = day_goal !== 0 ? (day_wc * 100 / day_goal) : 0;

                this.setState({
                    day_wc: day_wc,
                    day_goal: day_goal,
                    day_percentage: day_percentage.toFixed(0),
                });
            });
    }

    render()
    {
        return(
            <div className="stats_wrapper">
                <h3>Statistics of the day</h3>
                <ul>
                    <li>Day's goal: <b>{this.state.day_goal}</b> words</li>
                    <li>Done: <b>{this.state.day_wc}</b> ({this.state.day_percentage}%)</li>
                </ul>
            </div>
        );
    }
}