import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, NavLink, Switch, Redirect} from "react-router-dom";
import axios from 'axios';
import * as Constants from './includes/constants';

import UserLogin from './user/user_login';
import UserSignup from './user/user_signup';
import Words from './words/words';
import Projects from './projects/projects';

import * as serviceWorker from './serviceWorker';

const Menu = (props) => {
    if (props.authed) {
        return (
            <nav id="navbar" className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <NavLink exact activeClassName="active" to="/words" className="nav-link">Words</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink exact activeClassName="active" to="/projects" className="nav-link">Projects</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink exact activeClassName="active" to="/profile" className="nav-link">Profile</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/logout" className="nav-link">Log out</NavLink>
                    </li>
                </ul>
            </nav>
        );
    } else {
        return (
            <nav id="navbar" className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <NavLink exact activeClassName="active" to="/login" className="nav-link">Log in</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink exact activeClassName="active" to="/signup" className="nav-link">Sign up</NavLink>
                    </li>
                </ul>
            </nav>
        );
    }
};

const PrivateRoute = ({component: Component, userToken}) => {
    return (
        <Route render={(props) => (
            userToken
                ? <Component userToken = {userToken} />
                : <Redirect to = "/login" />
        )} />
    );
};

const Header = () => (
    <header className="jumbotron">
        <div className="container">
            <h1 className="display-3">Now that's a header</h1>
        </div>
    </header>
);

class Page extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            message: {
                type: null,
                content: null,
            },
            user: {
                username: null,
                token: null,
            },
            authed: false,
        }

        this.userLoggedIn = this.userLoggedIn.bind(this);
    }

    componentDidMount()
    {
        const token = localStorage.getItem('token');

        if (token) {
            axios.post(
                    `${Constants.public_url}/loginWithToken`,
                    {token: token},
                    {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
                )
                .then(res => this.setState({
                    user: {
                        username: res.data.username,
                        token: token
                    },
                    authed: true,
                }))
                .catch(error => localStorage.removeItem('token'));
        }
    }

    userLoggedIn(res)
    {
        this.setState({
            message: {
                type: 'success',
                content: res.message,
            },
            user: {
                username: res.username,
                token: res.token,
            },
            authed: true,
        });

        localStorage.setItem('token', res.token);
    }

    logout()
    {
        axios.get(
            `${Constants.api_url}/user/logout`,
            {headers: {Authorization: this.state.user.token}}
        )
        .then(res => {
            localStorage.removeItem('token');
            this.setState({
                message: {
                    type: 'success',
                    content: 'Logout successful',
                },
                user: {
                    username: null,
                    token: null,
                },
                authed: false,
            });
        })
        .catch(error => this.setState({
            'message': {
                'type': 'error',
                'content': 'Logging out failed'
            }
        }));
    }

    render()
    {
        if (this.state.authed) {
            return (
                <Router>
                    <Menu authed = {this.state.authed} />
                    <Header />
                    <div className="container">
                        <Switch>
                            <PrivateRoute path = "/words" component = {Words} userToken = {this.state.user.token} />
                            <PrivateRoute path = "/projects" component = {Projects} userToken = {this.state.user.token} />
                            <Route path = "/logout" component = {() => this.logout()} />
                            <Redirect to={{pathname: '/words'}} />}
                        </Switch>
                    </div>
                </Router>
            );
        } else {
            return (
                <Router>
                    <Menu authed = {this.state.authed} />
                    <Header />
                    <div className="container">
                        <Switch>
                            <Route path="/login" render={
                                props => <UserLogin userLoggedIn = {this.userLoggedIn} />
                            } />
                            <Route path="/signup" component={UserSignup} />
                            <Redirect to={{pathname: '/login'}} />}
                        </Switch>
                    </div>
                </Router>
            );
        }
    }
}

ReactDOM.render(<Page />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
