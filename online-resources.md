# React

- [Official docs](https://reactjs.org/docs/forms.html#controlled-components)
- [How to submit forms](https://html5hive.org/how-to-submit-forms-and-save-data-with-react-js-and-node-js/)
- [CRUD React](https://www.codeofaninja.com/2016/07/react-crud-tutorial.html)
- [Structuring a React project](https://blog.bitsrc.io/structuring-a-react-project-a-definitive-guide-ac9a754df5eb)
- [React Router and protected routes](https://tylermcginnis.com/react-router-protected-routes-authentication/)

# SLIM PHP

- [Token based API with Slim PHP and PDO](http://www.webstreaming.com.ar/articles/php-slim-token-authentication/)
- [Slim Skeleton](https://github.com/slimphp/Slim-Skeleton)
- [JSON Web Token User Auth](https://arjunphp.com/secure-web-services-using-jwt-slim3-framework/)
- [Token authentication](http://www.webstreaming.com.ar/articles/php-slim-token-authentication/)
- [Test a REST API with cURL](https://www.baeldung.com/curl-rest)
- [Middlewares](http://www.slimframework.com/docs/v3/concepts/middleware.html)

# Librairies/concepts additionnel.le.s

- [Axios](https://github.com/axios/axios)
- [Bootstrap](https://getbootstrap.com/docs/4.3)
- [Eloquent](https://github.com/illuminate/database)
- [Using the Web Storage API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API)

# FAQ

## Généralités

### Penser du haut vers le bas plutôt que l'inverse

En React, c'est le composant tout en haut de la hiérarchie qui récupère les changements d'état de ses enfants et donne des instructions en conséquence. 

## Erreurs communes

### La fonction liée à un événement (par ex. `onClick`) s'exécute au chargement

- Avez-vous passé la référence de la fonction ou exécuté la fonction directement ?

**Passer la référence :**

`<button onClick = {() => this.handleClick()} />`

`<button onClick = {this.handleClick} />`

**Exécuter la fonction :**

`<button onClick = {this.handleClick()} />`

#### Import de fichier/classe