<?php

$container['UserController'] = function ($container) {
    return new \App\Controllers\UserController($container);
};
$container['WordsController'] = function ($container) {
    return new \App\Controllers\WordsController($container);
};
$container['StatsController'] = function ($container) {
    return new \App\Controllers\StatsController($container);
};
$container['ProjectsController'] = function ($container) {
    return new \App\Controllers\ProjectsController($container);
};