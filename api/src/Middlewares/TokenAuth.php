<?php

namespace App\Middlewares;

use App\Models\User;
use \Firebase\JWT\JWT;

class TokenAuth
{
    public function __invoke($request, $response, $next)
    {
        if (0 === strpos($request->getUri()->getPath(), '/api')) {
            $headers = $request->getHeaders();

            if (empty($headers['HTTP_AUTHORIZATION'])) {
                $response->withJson(['message' => 'Token is empty'])->withStatus(400);
            }

            $payload_header = explode(' ', $headers['HTTP_AUTHORIZATION'][0]);
            dd($payload_header);
            $decoded_payload = (array)JWT::decode($payload_header[1], getenv('JWT_SECRET'), [getenv('JWT_ALGORITHM')]);
            $user = User::getByToken($decoded_payload['token']);


            if (isset($request->getHeader('Authorization')[0])) {
                $token = $request->getHeader('Authorization')[0];
            }

            if (!empty($token)) {
                if (!empty($user = User::getByToken($auth_token))) {
                    $user->keepTokenAlive();
                } else {
                    $response->withStatus(401);
                }
            } else {
                $response->withStatus(401);
            }
        }

        $reponse = $next($request, $response);
        return $reponse;
    }
}