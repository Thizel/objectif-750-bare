<?php

use Slim\App;

return function (App $app) {
    $app->add(new \App\Middlewares\TokenAuth($request, $response, $next));

    $app->add(new \Tuupola\Middleware\JwtAuthentication([
        'path'      => '/api',
        'attribute' => 'decoded_token_data',
        'secret'    => getenv('JWT_SECRET'),
        'algorithm' => [getenv('JWT_ALGORITHM')],
        'secure'    => (bool)getenv('JWT_SECURE'),
        'error'     => function ($response, $arguments) {
            $data['status'] = 'error';
            $data['message'] = $arguments['message'];
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        }
    ]));
};
