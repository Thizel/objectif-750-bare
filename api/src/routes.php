<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

return function (App $app) {
    $container = $app->getContainer();

    $app->post('/login', \App\Controllers\UserController::class . ':login');
    $app->post('/signup', \App\Controllers\UserController::class . ':signup');
    $app->post('/loginWithToken', \App\Controllers\UserController::class . ':loginWithToken');

    $app->group('/api', function (\Slim\App $app) {
        $app->group('/user', function (\Slim\App $app) {
            $app->get('/logout', \App\Controllers\UserController::class . ':logout');
        });

        $app->group('/words', function (\Slim\App $app) {
            $app->get('/read', \App\Controllers\WordsController::class . ':read');
            $app->post('/submit', \App\Controllers\WordsController::class . ':submit');
        });

        $app->group('/stats', function (\Slim\App $app) {
            $app->get('/read', \App\Controllers\StatsController::class . ':read');
        });

        $app->group('/projects', function (\Slim\App $app) {
            $app->get('/read', \App\Controllers\ProjectsController::class . ':read');
            $app->post('/submit', \App\Controllers\ProjectsController::class . ':submit');
            $app->get('/delete/{project_id}', \App\Controllers\ProjectsController::class . ':delete');
            $app->get('/disable/{project_id}', \App\Controllers\ProjectsController::class . ':disable');
        });
    });
};
