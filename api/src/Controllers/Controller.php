<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface;

class Controller
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected function getInputData($request)
    {
        $input = $request->getBody();
        return json_decode($input, true);
    }
}