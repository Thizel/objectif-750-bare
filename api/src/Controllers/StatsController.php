<?php

namespace App\Controllers;

use App\Models\Stats;

class StatsController extends Controller
{
    public function read($request, $response)
    {
        // FIXME: replace 1 with actual user_id
        $stats = Stats::getOrCreateDaysStats(1);
        return $response->withJson(!empty($stats) ? ['day_wc' => $stats->word_count, 'day_goal' => $stats->word_goal] : []);
    }
}