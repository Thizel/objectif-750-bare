<?php

namespace App\Controllers;

use App\Models\Project;

class ProjectsController extends Controller
{
    public function read($request, $response)
    {
        // FIXME: replace 1 with actual user_id
        $projects_res = Project::select('id', 'name', 'description')->where('user_id', 1)->get();
        $projects = [];

        foreach ($projects_res as $project) {
            $projects[$project->id] = [
                'id'          => $project->id,
                'name'        => $project->name,
                'description' => $project->description,
            ];
        }

        return $response->withJson($projects);
    }

    public function submit($request, $response)
    {
        // FIXME: replace 1 with actual user_id
        $user_id = 1;
        $json = $request->getBody();
        $inputs = json_decode($json, true);

        if (empty($inputs['name']) || !empty(Project::where('name', $inputs['name'])->first())) {
            return $response->withJson(['message' => 'duplicate'])->withStatus(400);
        }

        // FIXME: control the inputs
        if (empty($inputs['id'])) {
            $project = new Project();
            $project->user_id = $user_id;
        } else {
            $project = Project::find($inputs['id']);
        }

        $project->name = !empty($inputs['name']) ? $inputs['name'] : null;
        $project->description = !empty($inputs['description']) ? $inputs['description'] : null;
        $project->save();
        return $response->withJson(['message' => 'ok']);
    }

    public function delete($request, $response)
    {
        $user_id = 1;
        $json = $request->getBody();
        $data = json_decode($json);
        $project = Project::where('user_id', $user_id)->where('id', $data['project_id'])->first();

        if (!empty($project)) {
            $project->delete();
        } else {
            return $response->withStatus(503);
        }
    }
}