<?php

namespace App\Controllers;

use App\Models\Words;
use App\Models\Stats;

class WordsController extends Controller
{
    public function read($request, $response)
    {
        $json = $request->getBody();
        $data = json_decode($json, true);
        dd($data);
        $words = Words::getTodaysWords(1);
        return $response->withJson(!empty($words) ? ['content' => $words->content, 'word_count' => $words->word_count] : []);
    }

    public function submit($request, $response)
    {
        $headers = $request->getHeaders();

        if (empty($headers['HTTP_AUTHORIZATION'])) {
            $response->withJson(['message' => 'Token is empty'])->withStatus(400);
        }

        $payload_header = explode(' ', $headers['HTTP_AUTHORIZATION'][0]);

        $decoded_payload = (array)JWT::decode($payload_header[1], getenv('JWT_SECRET'), [getenv('JWT_ALGORITHM')]);
        $user = User::getByToken($decoded_payload['token']);

        if (empty($user))

        $data = $this->getInputData($request);
        dd($data);

        if (empty($words = Words::getTodaysWords(1))) {
            $words = new Words();
            $words->user_id = $user_id;
        }

        $words->content = $data['content'] ?: null;
        $words->word_count = $data['content'] ? countWords($data['content']) : 0;
        $words->save();

        // Update stats
        $stats = Stats::getOrCreateDaysStats($user_id);

        if (!empty($stats)) {
            $new_word_count = Words::getDaysWordCount($user_id);
            $stats->word_count = $new_word_count;
            $stats->save();
        }

        return $response->withJson(['message' => 'ok']);
    }
}