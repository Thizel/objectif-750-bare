<?php

namespace App\Controllers;

use \Firebase\JWT\JWT;
use Tuupola\Base62;

use App\Models\User;

class UserController extends Controller
{
    public function signup($request, $response)
    {
        $input = $request->getBody();
        $data = json_decode($input, true);

        if (empty($data['username']) || empty($data['password'])) {
            return $response->withJson(['message' => 'Username and password can\'t be empty'])->withStatus(400);
        }

        if ($data['password'] !== $data['password_repeat']) {
            return $response->withJson(['message' => 'Passwords don\'t match'])->withStatus(400);
        }

        if (User::where('username', $data['username'])->first()) {
            return $response->withJson(['message' => 'This username already exists'])->withStatus(400);
        }

        $user = new User();
        $user->username = $data['username'];
        $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
        $user->save();

        return !empty($user->id) ?
            $response->withJson(['message' => 'Registration successful']) :
            $response->withJson(['message' => 'Error'])->withStatus(503);
    }

    public function login($request, $response)
    {
        $data = json_decode($request->getBody(), true);

        if (empty($data['username']) || empty($data['password'])) {
            return $response->withJson(['message' => 'Login and password can\'t be empty'])->withStatus(400);
        }

        try {
            $user = User::where('username', $data['username'])->first();
        } catch (Exception $e) {
            return $response->withJson(['message' => $e->getMessage()])->withStatus(401);
        }

        if (empty($user)) {
            return $response->withJson(['message' => 'Incorrect username'])->withStatus(401);
        }
        if (!password_verify($data['password'], $user->password)) {
            return $response->withJson(['message' => 'Incorrect password'])->withStatus(401);
        }

        $base62 = new Base62;

        $payload = [
            'token' => $base62->encode(random_bytes(16)),
            'now' => new \Datetime(),
            'expiration' => new \DateTime("now +2 hours"),
        ];

        $token = JWT::encode($payload, getenv('JWT_SECRET'), getenv('JWT_ALGORITHM'));
        $user->token = $payload['token'];
        $user->token_expiration = $payload['expiration'];
        $user->save();

        return $response->withJson(['token' => $token]);
    }

    public function loginWithToken($request, $response)
    {
        $data = json_decode($request->getBody(), true);

        if (empty($data['token'])) {
            $response->withJson(['message' => 'Token is empty'])->withStatus(400);
        }

        $decoded_payload = (array)JWT::decode($data['token'], getenv('JWT_SECRET'), [getenv('JWT_ALGORITHM')]);
        $user = User::getByToken($decoded_payload['token']);

        if (!empty($user)) {
            $user->keepTokenAlive();
            return $response->withJson(['username' => $user->username]);
        }

        return $response->withJson(['message' => 'Token expired or is invalid'])->withStatus(401);
    }

    public function logout($request, $response)
    {
        $token = $request->getHeaders('Authorization');
        $data = json_decode($request->getBody(), true);

        if (empty($data['token'])) {
            return $response->withJson(['message' => 'Token cannot be empty'])->withStatus(401);
        }

        $user = User::where('token', $data['token'])->where('token_expiration', '>=', $now)->first();

        if (empty($user)) {
           return $response->withJson(['message' => 'Token expired or is invalid'])->withStatus(401);
        }

        $user->token = null;
        $user->token_expiration = null;
        $user->save();
        return $response->withJson(['message' => 'Successfully logged out']);

    }
}