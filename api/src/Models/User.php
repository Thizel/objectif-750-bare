<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    protected $fillable = ['id', 'username', 'password', 'word_goal', 'token', 'token_expiration'];
    public $timestamps = false;

    public static function getByToken($token)
    {
        if (empty($token)) {
            return false;
        }

        $now = date('Y-m-d H:i:s');
        $user = User::where('token', $token)->where('token_expiration', '>=', $now)->first();

        return !empty($user) ? $user : false;
    }

    public function keepTokenAlive()
    {
        $this->token_expiration = date('Y-m-d H:i:s', strtotime('+2 hour'));
        $this->save();
    }

    public static function getCurrentWordGoal($user_id)
    {
        $user = self::select('word_goal')->where('id', $user_id)->first();
        return !empty($user) ? $user->word_goal : 0;
    }
}
