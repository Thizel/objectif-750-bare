<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Words extends Model
{
    protected $table = 'words';
    protected $fillable = ['title', 'content', 'word_count', 'category_id'];
    public $timestamps = true;

    public static function getTodaysWords($user_id)
    {
        $now = date('Y-m-d') . ' 00:00:00';
        $words = self::where('created_at', '>=', $now)->where('user_id', $user_id)->first();

        return $words;
    }

    private function countWords($words)
    {
        return preg_match_all('%[a-zA-Z0-9\-_]+[\s,?!.\'";:*_\/()]*%', $words, $matches);
    }

    public static function getDaysWordCount($user_id)
    {
        $word_count = 0;
        $now = date('Y-m-d') . ' 00:00:00';
        $words = self::select('word_count')
            ->where('created_at', '>=', $now)
            ->where('user_id', $user_id)
            ->get();

        foreach ($words as $word) {
            $word_count+= $word->word_count;
        }

        return $word_count;
    }
}