<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

/**
 *
 */
class Stats extends Model
{
    protected $table = 'stats';
    protected $fillable = ['user_id', 'word_goal', 'word_count', 'date'];
    public $timestamps = true;

    public static function getOrCreateDaysStats($user_id)
    {
        $date = date('Y-m-d') . ' 00:00:00';
        $stats = self::where('user_id', $user_id)
            ->where('created_at', '>=', $date)
            ->first();

        if (empty($stats) && !empty($word_goal = (new User())::getCurrentWordGoal($user_id))) {
            $stats = new self;
            $stats->word_count = 0;
            $stats->user_id = $user_id;
            $stats->word_goal = $word_goal;
            $stats->save();
        }

        return $stats;
    }
}